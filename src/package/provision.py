from argparse import ArgumentParser
from logging import debug, getLogger, warning
from os import chdir, environ
from pathlib import Path
from shutil import copy2, copytree
from subprocess import run  # nosec: B404

PATHS_COPY = [
    ".gitattributes",
    ".gitignore",
    "pyproject.toml",
    "k8s/Containerfile.templatepython",
    ".gitlab-ci.yml",
    "README.md",
]

PATHS_SYMLINK = [
    ".containerignore",
    ".editorconfig",
    ".lefthook.yml",
    ".markdownlint.json",
    ".mega-linter.yml",
    ".prettierignore",
    ".prettierrc.json",
    ".sourcery.yaml",
    ".v8rrc.yml",
    ".vale.ini",
    "styles/",
    "renovate.json",
    "lychee.toml",
    "pdm.toml",
]

if __name__ == "__main__":
    getLogger().setLevel(level=environ.get("LOG_LEVEL", "INFO").upper())
    argumentparser = ArgumentParser(
        prog="Template provisioner",
        description="Produces a Git-based work product, such as a program or documentation.",
    )
    argumentparser.add_argument(
        "path_directory_product",
        help="Directory to create product in.",
        type=Path,
    )
    namespace = argumentparser.parse_args()
    namespace.path_directory_product = namespace.path_directory_product.absolute().resolve()
    input(f"Press any key to continue filesystem changes under {namespace.path_directory_product:s} ...")
    namespace.path_directory_product.mkdir()
    chdir(namespace.path_directory_product)
    path_dir_templatepython = (namespace.path_directory_product / "template_Python").relative_to(
        namespace.path_directory_product,
    )
    run(
        ["git", "init", "--initial-branch=main"],  # noqa: S603 S607
        check=True,
    )
    run(
        [  # noqa: S603 S607
            "git",
            "submodule",
            "add",
            "--branch",
            "main",
            "--",
            "git@gitlab.com:han-aim/research/templates/python",
            path_dir_templatepython.name,
        ],
        check=True,
    )
    run(
        [  # noqa: S603 S607
            "git",
            "submodule",
            "update",
            "--init",
            "--recursive",
        ],
        check=True,
    )
    run(
        ["git", "checkout", "main"],  # noqa: S603 S607
        check=True,
        cwd=path_dir_templatepython / "template_Kubernetes",
    )
    for name_file in PATHS_COPY:
        path_original = path_dir_templatepython / name_file
        path_new = namespace.path_directory_product / name_file
        if not path_new.exists():
            try:
                if target_is_directory := name_file.endswith("/"):
                    copytree(
                        src=path_original,
                        dst=path_new,
                    )
                    debug("'%s' copied as subtree to '%s'", path_original, path_new)
                else:
                    if "/" in name_file:
                        path_new.parent.mkdir(parents=True)
                    copy2(
                        src=path_original,
                        dst=path_new,
                    )
                    debug("%s copied to %s ", path_original, path_new)
            except FileExistsError as exception:
                warning(exception)
    for name_file in PATHS_SYMLINK:
        try:
            path_symlink_from = namespace.path_directory_product / name_file
            path_to = path_dir_templatepython / name_file
            path_symlink_from_relativetoroot = path_symlink_from.relative_to(namespace.path_directory_product)
            path_symlink_to = (
                Path().joinpath(*[".." for _ in path_symlink_from_relativetoroot.parent.parts]) / path_to
                if len(path_symlink_from_relativetoroot.parts) > 1
                else path_to
            )
            path_symlink_from.symlink_to(
                path_symlink_to,
                target_is_directory=name_file.endswith("/"),
            )
            debug("%s links to %s", path_symlink_from, path_symlink_to)
        except FileExistsError as exception:
            warning(exception)
