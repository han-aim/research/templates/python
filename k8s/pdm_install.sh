#!/bin/sh
set -eux
path_file_script="$(mktemp)"
curl \
  --fail \
  --location 'https://raw.githubusercontent.com/pdm-project/pdm/main/install-pdm.py' \
  --proto '=https' \
  --show-error \
  --silent \
  --tlsv1.3 \
  >"${path_file_script:?}"
python3 \
  -- \
  "${path_file_script:?}" \
  -v '2.9.3'
