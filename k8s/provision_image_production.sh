#!/bin/sh
set -eux
apk \
  add \
  curl~=8 \
  openblas \
  python3=~=3.11 \
  py3-pandas
